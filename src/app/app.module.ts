import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { HomeModule } from './home/home.module';


@NgModule({
  imports: [
    BrowserModule,
    CoreModule,
    SharedModule,
    PageNotFoundModule,
    HomeModule,
    NgbModule.forRoot()


  ],
  declarations: [
    AppComponent
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
